package br.com.rodrigotoledo.jdbc.teste;

import java.util.Calendar;

import br.com.rodrigotoledo.jdbc.dao.ContatoDao;
import br.com.rodrigotoledo.jdbc.modelo.Contato;

public class TestaInsere {
	public static void main(String[] args) {
		//pronto para gravar
		Contato contato = new Contato();
		contato.setNome("Rodrigo Toledo");
		contato.setEmail("rodrigotoledo7@gmail.com");
		contato.setEndereco("Rua Guiana, 2904");
		contato.setDataNascimento(Calendar.getInstance());
		
		//grave nessa conexão
		ContatoDao dao = new ContatoDao();
		
		//metodo elegante
		dao.adiciona(contato);
		
		System.out.println("Gravado!");
	}

}
