package br.com.rodrigotoledo.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	
	public Connection getConnection() {
		try {
			return DriverManager.getConnection("jdbc:mysql://localhost/fj21?useSSL=false", "root", "123321");
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}

}
